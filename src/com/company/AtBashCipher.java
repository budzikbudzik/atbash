package com.company;

public class AtBashCipher implements Cipher {
    char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    char[] alphabetReversed = "zyxwvutsrqponmlkjihgfedcba".toCharArray();
    char[] alphabetUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    char[] alphabetReversedUpperCase = "ZYXWVUTSRQPONMLKJIHGFEDCBA".toCharArray();

    /**
     * deszyfrowanie wiadomosci
     */
    public String decode(final String message) {
        String outcomeString = "";
        String input = message;

        for (int i = 0; i < input.length(); i++) {
            for (int j = 0; j < alphabet.length; j++) {
                if (input.charAt(i) == alphabet[j]) {
                    outcomeString += alphabetReversed[j];
                } else if (input.charAt(i) == alphabetUpperCase[j]) {
                    outcomeString += alphabetReversedUpperCase[j];
                }
            }
            if (input.charAt(i) == 32) {
                outcomeString += " ";
            }
        }
        return outcomeString;
    }

    /**
     * szyfrowanie wiadomosci
     */
    public String encode(final String message) {
        String outcomeString = decode(message);

        return outcomeString;
    }
}
